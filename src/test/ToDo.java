package test;

import java.util.Date;

public class ToDo implements ComparabilDupaData{
	private String descriere;
	private Integer durata;
	private Date termenLimita;
	
	public ToDo() {
		
	}
	
	public ToDo(String descriere, Integer durata, Date termenLimita) {
		super();
		this.descriere = descriere;
		this.durata = durata;
		this.termenLimita = termenLimita;
	}
	
	public String getDescriere() {
		return descriere;
	}
	public void setDescriere(String descriere) {
		this.descriere = descriere;
	}
	public Integer getDurata() {
		return durata;
	}
	public void setDurata(Integer durata) {
		this.durata = durata;
	}
	public Date getTermenLimita() {
		return termenLimita;
	}
	public void setTermenLimita(Date termenLimita) {
		this.termenLimita = termenLimita;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("ToDo [ descriere= " + descriere + ", durata= " + durata + " minute" + ", termenLimita= " + termenLimita + " ]");
		return stringBuilder.toString();
	}

	@Override
	public String comparaToDoDupaData(ToDo t) {
		 int decizie = getTermenLimita().compareTo(t.getTermenLimita());
		  if(decizie == 1) {
			  return this.descriere + " are data limita mai tarziu decat " + t.getDescriere();
		  }else if(decizie == -1){
			  return this.descriere + " are data limita mai devreme decat " + t.getDescriere();
		  }else {
			  return "Au aceeasi data limita!";
		  }
	}
	
	

}
