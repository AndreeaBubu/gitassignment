package test;

import java.util.Date;

public class TestGit {
	public static void main(String [] args) {
		System.out.println("Hello Git ! Denumirea proiectului de licenta este Aplicatie mobila pentru gestiunea eficienta a timpului");
		ToDo toDo2 = new ToDo("cts assignment", 45, new Date());
		@SuppressWarnings("deprecation")
		ToDo toDo = new ToDo("capitol 1 licenta", 3600, new Date(120, 3, 24, 20, 300));
		System.out.println("\n" + toDo);
		System.out.println("\n" + toDo2);
		
		System.out.println("\n" + toDo.comparaToDoDupaData(toDo2));
	}
}
